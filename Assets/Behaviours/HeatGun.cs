﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class HeatGun : NetworkBehaviour {
    [SyncVar]
    public bool firing = false;

    public LineRenderer lineRenderer;
    NetworkIdentity netIdentity;
	// Use this for initialization
	void Start () {
        netIdentity = GetComponent<NetworkIdentity>();
        if (netIdentity.isLocalPlayer) {
            StartCoroutine(damage());
        }
	}

    void Update() {
        if (netIdentity.isLocalPlayer) {
            if (netIdentity.isClient) {
                CmdSetFiring(Input.GetButton("Fire1"));
            } else { 
                firing = Input.GetButton("Fire1");
            }
        }
        lineRenderer.enabled = firing;
    }

    [Command]
    void CmdSetFiring(bool f) {
        this.firing = f;
    }

    IEnumerator damage() {
        while (true) {
            if (firing) {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.forward, out hit)) {
                    Explodable explodable = hit.collider.GetComponent<Explodable>();
                    if (explodable != null) {
                        explodable.CmdAddHeat(1f);
                    }
                }
            }
            yield return new WaitForSeconds(0.2f);
        }
    }
}
