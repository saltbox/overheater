﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerMoveAndTurn: NetworkBehaviour {
    float playerSpeed = 8f;
    float cameraTurnSpeed = 1f;
    CharacterController characterController;
    NetworkTransform networkTransform;
	// Use this for initialization
	void Start () {
        characterController = GetComponent<CharacterController>();
        networkTransform = GetComponent<NetworkTransform>();
        if (isLocalPlayer) {
            transform.GetComponentInChildren<Camera>().enabled = true;
            transform.GetComponentInChildren<AudioListener>().enabled = true;
        } else {
            transform.GetComponentInChildren<Camera>().enabled = false;
            transform.GetComponentInChildren<AudioListener>().enabled = false;
        }
	}

    void Update() {
        if (!isLocalPlayer) {
            return;
        } else {
            characterController.SimpleMove(transform.rotation * new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")).normalized * playerSpeed);
            transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X") * cameraTurnSpeed, 0));
        }
    }
}
