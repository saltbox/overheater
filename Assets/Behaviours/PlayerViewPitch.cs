﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerViewPitch : MonoBehaviour {
    public bool isLocalPlayer;
    float sensitivity = 1f;
	// Use this for initialization
	void Start () {
        isLocalPlayer = GetComponentInParent<NetworkIdentity>().isLocalPlayer;
	}
	
	// Update is called once per frame
	void Update () {
        if (isLocalPlayer) {
            transform.Rotate(new Vector3(-Input.GetAxis("Mouse Y") * sensitivity, 0), Space.Self);
        }
        
	}
}
