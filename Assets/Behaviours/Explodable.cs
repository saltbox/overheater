﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Explodable : NetworkBehaviour {
    public GameObject explosionPrefab;
    [SyncVar]
    public float maxHeat = 100f;
    [SyncVar]
    public float heat = 0f;

    float heatLerped;

    Renderer renderer;
    public GameObject model;
	// Use this for initialization
	void Start () {
        renderer = model.GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (isServer) {
            if (heat >= 0) {
                heat -= Time.deltaTime * 1;
            } else {
                heat = 0;
            }
        }

        heatLerped = Mathf.Lerp(heatLerped, heat, 0.01f);
        renderer.material.color = new Color((heat / maxHeat) / 2 + 0.5f, 0.5f, 0.5f);
        model.transform.localScale = Vector3.one * Mathf.Max(1, (heatLerped / maxHeat) + 0.3f);

        if (heat >= maxHeat) {
            if (isServer) {
                RpcSpawnExplosion();
                SpawnExplosion();
            }
            GameObject.Destroy(gameObject);
        }
	}

    [Command]
    public void CmdAddHeat(float heat) {
        this.heat += heat;
    }

    [ClientRpc]
    void RpcSpawnExplosion() {
        SpawnExplosion();
    }

    void SpawnExplosion() {
        GameObject.Instantiate(explosionPrefab).transform.position = transform.position;
    }
}
