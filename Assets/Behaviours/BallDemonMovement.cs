﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;
using UnityEngine.Networking;

public class BallDemonMovement : NetworkBehaviour {
    public GameObject targetPlayer;

    GameObject[] players;
    NetworkIdentity netIdentity;

    [SyncVar]
    float speed = 5f;

	// Use this for initialization
	void Start () {
        netIdentity = GetComponent<NetworkIdentity>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        
	}

    void Update() {
        if (targetPlayer == null) {
            findTarget();
        } else {
            //speed += 0.5f * Time.deltaTime;
            transform.position += (targetPlayer.transform.position - transform.position).normalized * Time.deltaTime * speed;
        }
    }

    public void findTarget() {
        players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length > 0) {
            targetPlayer = players[0];
        }
    }
}
