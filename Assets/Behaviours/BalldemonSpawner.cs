﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;
using UnityEngine.Networking;

public class BalldemonSpawner : MonoBehaviour {
    public GameObject demonPrefab;

    GameObject[] players;
    Vector3 centerOfMass;
    float spawnTime = 10, spawnTimer = 9;
    float spawnDistance = 15;
	// Use this for initialization
	void Start () {
        StartCoroutine(checkForNewPlayers());
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        spawnTimer += Time.fixedDeltaTime;
        if (spawnTimer >= spawnTime) {
            spawnTimer = 0;
            spawnTime -= 0.1f;
            calculateCenterOfMass();
            spawnDemon();
        }
	}

    IEnumerator checkForNewPlayers() {
        while (true) {
            players = GameObject.FindGameObjectsWithTag("Player");
            yield return new WaitForSeconds(5f);
        }
    }

    private void calculateCenterOfMass() {
        centerOfMass = Vector3.zero;
        foreach (GameObject p in players) {
            centerOfMass += p.transform.position;
            centerOfMass /= players.Length;
        }
    }

    private void spawnDemon() {
        Vector3 targetPos = centerOfMass;
        targetPos += Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)) * Vector3.forward * spawnDistance;
        GameObject newDemon = GameObject.Instantiate(demonPrefab);
        newDemon.transform.position = targetPos;
        NetworkServer.Spawn(newDemon);
    }
}
