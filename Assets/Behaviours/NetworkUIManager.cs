﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkUIManager : MonoBehaviour {
    public Camera menuCamera;

    NetworkManagerHUD hud;
    NetworkManager manager;
	// Use this for initialization
	void Start () {
        hud = GetComponent<NetworkManagerHUD>();
        manager = GetComponent<NetworkManager>();
	}
	
	// Update is called once per frame
	void Update () {
        if (hud.manager.isNetworkActive) {
            hud.showGUI = false;
            menuCamera.enabled = false;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        if (Input.GetButtonDown("Cancel")) {
            manager.StopClient();
            manager.StopServer();
            hud.showGUI = true;
            menuCamera.enabled = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
	}
}
