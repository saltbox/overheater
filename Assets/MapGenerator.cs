﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;
using UnityEngine.Networking;
using UnityStandardAssets.Utility;

public class MapGenerator : NetworkBehaviour {
    public GameObject[] prefabs;
    int segmentsGenerated = 1;
    [Command]
    public void CmdGenerate() {
        int segmentToGenerate = Random.Range(0, prefabs.Length);
        RpcGenerate(segmentToGenerate);
    }

    [ClientRpc]
    public void RpcGenerate(int segment) {
        Generate(segment);
    }

    public void Generate(int segment) {
        GameObject newMapSegment = GameObject.Instantiate(prefabs[segment]);
        newMapSegment.transform.position = new Vector3(segmentsGenerated * 64, 0, 0);
        newMapSegment.GetComponentInChildren<ActivateTrigger>().target = gameObject;
        segmentsGenerated++;
    }

    public void DoActivateTrigger() {
        CmdGenerate();
    }
}
